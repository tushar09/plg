package com.example;

import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Schema;

public class Generator {
    private static final String PROJECT_DIR = System.getProperty("user.dir");
    private static final int DB_VERSION = 13;
    private static final String DEFAULT_PACKAGE = "triumphit.tech.plg.db";

    public static void main(String [] args){
        Schema schema = new Schema(DB_VERSION, DEFAULT_PACKAGE);
        schema.enableKeepSectionsByDefault();

        addTables(schema);

        try {
            new DaoGenerator().generateAll(schema, PROJECT_DIR  + "//app//src//main//java");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void addTables(Schema schema) {
        addUniqLocation(schema);
        addPredictedLocation(schema);
        addPredictedLocationHistory(schema);
    }

    private static Entity addPredictedLocationHistory(Schema schema){
        Entity predictedLocationHistory = schema.addEntity("PredictedLocationHistory");
        predictedLocationHistory.addLongProperty("id").primaryKey().autoincrement().unique();
        predictedLocationHistory.addIntProperty("time");
        predictedLocationHistory.addStringProperty("day");
        predictedLocationHistory.addStringProperty("latlng");

        return predictedLocationHistory;
    }

    private static Entity addPredictedLocation(Schema schema) {
        Entity predictedLocation = schema.addEntity("PredictedLocation");
        predictedLocation.addLongProperty("id").primaryKey().autoincrement().unique();
        predictedLocation.addIntProperty("time");
        predictedLocation.addStringProperty("MONDAY");
        predictedLocation.addStringProperty("TUESDAY");
        predictedLocation.addStringProperty("WEDNESDAY");
        predictedLocation.addStringProperty("THURSDAY");
        predictedLocation.addStringProperty("FRIDAY");
        predictedLocation.addStringProperty("SATERDAY");
        predictedLocation.addStringProperty("SUNDAY");

        return predictedLocation;

    }

    private static Entity addUniqLocation(Schema schema) {
        Entity uniqLocation = schema.addEntity("UniqLocation");
        uniqLocation.addLongProperty("id").primaryKey().autoincrement();
        uniqLocation.addStringProperty("place_name");
        uniqLocation.addStringProperty("latLng").unique();

        return uniqLocation;
    }

}
