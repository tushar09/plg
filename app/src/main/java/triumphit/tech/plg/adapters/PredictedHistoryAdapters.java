package triumphit.tech.plg.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import triumphit.tech.plg.R;
import triumphit.tech.plg.databinding.RowBinding;
import triumphit.tech.plg.utils.Extractor;

/**
 * Created by Tushar on 8/1/2017.
 */

public class PredictedHistoryAdapters extends BaseAdapter{

    private Context context;
    private List<Extractor> predictedLocationList;
    private String dayName;

    public PredictedHistoryAdapters(Context context, List<Extractor> predictedLocationList, String dayName){
        this.context = context;
        this.predictedLocationList = predictedLocationList;
        this.dayName = dayName;
    }

    @Override
    public int getCount(){
        return predictedLocationList.size();
    }

    @Override
    public Object getItem(int i){
        return predictedLocationList.get(i);
    }

    @Override
    public long getItemId(int i){
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup){
        Holder holder;
        if(view == null){
            holder = new Holder(context);
            view = holder.binding.getRoot();
            view.setTag(holder);
        }else {
            holder = (Holder) view.getTag();
        }

        holder.binding.tvTime.setText(predictedLocationList.get(position).getTime() + " Hours");
        holder.binding.tvLatlong.setText(predictedLocationList.get(position).getLatlng());
        holder.binding.tvDay.setText(predictedLocationList.get(position).getDayName());

        return view;
    }

    class Holder{
        RowBinding binding;
        public Holder(Context context){
            binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.row, null, true);
        }
    }
}
