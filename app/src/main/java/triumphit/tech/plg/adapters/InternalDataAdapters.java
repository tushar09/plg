package triumphit.tech.plg.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import triumphit.tech.plg.R;
import triumphit.tech.plg.databinding.RowBinding;
import triumphit.tech.plg.databinding.RowInternalDataBinding;
import triumphit.tech.plg.db.UniqLocation;
import triumphit.tech.plg.utils.Extractor;

/**
 * Created by Tushar on 8/1/2017.
 */

public class InternalDataAdapters extends BaseAdapter{

    private Context context;
    private List<UniqLocation> uniqLocationList;
    private String dayName;

    public InternalDataAdapters(Context context, List<UniqLocation> uniqLocationList){
        this.context = context;
        this.uniqLocationList = uniqLocationList;
    }

    @Override
    public int getCount(){
        return uniqLocationList.size();
    }

    @Override
    public Object getItem(int i){
        return uniqLocationList.get(i);
    }

    @Override
    public long getItemId(int i){
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup){
        Holder holder;
        if(view == null){
            holder = new Holder(context);
            view = holder.binding.getRoot();
            view.setTag(holder);
        }else {
            holder = (Holder) view.getTag();
        }

        holder.binding.tvTime.setText((position + 1) + "");
        holder.binding.tvLatlong.setText(uniqLocationList.get(position).getLatLng().replaceAll(",", "\n"));
        //holder.binding.tvDay.setText(uniqLocationList.get(position).getPlace_name());

        return view;
    }

    class Holder{
        RowInternalDataBinding binding;
        public Holder(Context context){
            binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.row_internal_data, null, true);
        }
    }
}
