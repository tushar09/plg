/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triumphit.tech.plg.predictiveLocationGeneration;

/**
 *
 * @author khaled
 */
import java.util.*;
import java.lang.*;
import java.io.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import static java.lang.System.out;

public class PredictionBasement {

    //############################################################################
    PredictionBasement() {

    }
    //############################################################################

    public static String make_Prediction(int time, double lat, double lon) {

        int flag = 0;
        String cor = "" + lat + ", " + lon;
        double initial_Value = 1500000000000000.0;
        String initial_String = "";
//********************************************************************************
        if (time == 0) {

            String temp_String = "";
            double temp_Value = 0.0;

//            double store[] = new double[PredictiveLocationGeneration.one.size() + 1];
            for (int i = 0; i < PredictiveLocationGeneration.one.size(); i++) {

                String sf = PredictiveLocationGeneration.one.get(i).toString();
                List<String> horse = Arrays.asList(sf.split(", "));

                double lat2 = Double.parseDouble(horse.get(0));
                double lon2 = Double.parseDouble(horse.get(1));
                double dist = UniqueCordinates.rangeCalculation(lat, lon, lat2, lon2);
//                            out.println(i+" >>>"+dist);
                out.println("@@@@@@@@@@@@@@@@ " + cor + " to " + sf + " ---> " + dist + " @@@@@@@@@@@@@@@@@@@@@");
                temp_Value = dist;
                temp_String = sf;

                if (dist > 0.115) {
                    flag = 1;
                    if (temp_Value < initial_Value) {
                        initial_Value = temp_Value;
                        initial_String = temp_String;
                    }
                }

            }

        } //*****************************************************************************    
        else if (time == 1) {

            String temp_String = "";
            double temp_Value = 0.0;

//            double store[] = new double[PredictiveLocationGeneration.one.size() + 1];
            for (int i = 0; i < PredictiveLocationGeneration.two.size(); i++) {

                String sf = PredictiveLocationGeneration.two.get(i).toString();
                List<String> horse = Arrays.asList(sf.split(", "));

                double lat2 = Double.parseDouble(horse.get(0));
                double lon2 = Double.parseDouble(horse.get(1));
                double dist = UniqueCordinates.rangeCalculation(lat, lon, lat2, lon2);
//                            out.println(i+" >>>"+dist);
                out.println("@@@@@@@@@@@@@@@@ " + cor + " to " + sf + " ---> " + dist + " @@@@@@@@@@@@@@@@@@@@@");
                temp_Value = dist;
                temp_String = sf;

                if (dist > 0.115) {
                    flag = 1;
                    if (temp_Value < initial_Value) {
                        initial_Value = temp_Value;
                        initial_String = temp_String;
                    }
                }

            }

        } //*******************************************************************************     
        else if (time == 2) {
            String temp_String = "";
            double temp_Value = 0.0;

//            double store[] = new double[PredictiveLocationGeneration.one.size() + 1];
            for (int i = 0; i < PredictiveLocationGeneration.three.size(); i++) {

                String sf = PredictiveLocationGeneration.three.get(i).toString();
                List<String> horse = Arrays.asList(sf.split(", "));

                double lat2 = Double.parseDouble(horse.get(0));
                double lon2 = Double.parseDouble(horse.get(1));
                double dist = UniqueCordinates.rangeCalculation(lat, lon, lat2, lon2);
//                            out.println(i+" >>>"+dist);
                out.println("@@@@@@@@@@@@@@@@ " + cor + " to " + sf + " ---> " + dist + " @@@@@@@@@@@@@@@@@@@@@");
                temp_Value = dist;
                temp_String = sf;

                if (dist > 0.115) {
                    flag = 1;
                    if (temp_Value < initial_Value) {
                        initial_Value = temp_Value;
                        initial_String = temp_String;
                    }
                }

            }

        } //****************************************************************************
        else if (time == 3) {
            String temp_String = "";
            double temp_Value = 0.0;

//            double store[] = new double[PredictiveLocationGeneration.one.size() + 1];
            for (int i = 0; i < PredictiveLocationGeneration.four.size(); i++) {

                String sf = PredictiveLocationGeneration.four.get(i).toString();
                List<String> horse = Arrays.asList(sf.split(", "));

                double lat2 = Double.parseDouble(horse.get(0));
                double lon2 = Double.parseDouble(horse.get(1));
                double dist = UniqueCordinates.rangeCalculation(lat, lon, lat2, lon2);
//                            out.println(i+" >>>"+dist);
                out.println("@@@@@@@@@@@@@@@@ " + cor + " to " + sf + " ---> " + dist + " @@@@@@@@@@@@@@@@@@@@@");
                temp_Value = dist;
                temp_String = sf;

                if (dist > 0.115) {
                    flag = 1;
                    if (temp_Value < initial_Value) {
                        initial_Value = temp_Value;
                        initial_String = temp_String;
                    }
                }

            }

        } //*****************************************************************************
        else if (time == 4) {

            String temp_String = "";
            double temp_Value = 0.0;

//            double store[] = new double[PredictiveLocationGeneration.one.size() + 1];
            for (int i = 0; i < PredictiveLocationGeneration.five.size(); i++) {

                String sf = PredictiveLocationGeneration.five.get(i).toString();
                List<String> horse = Arrays.asList(sf.split(", "));

                double lat2 = Double.parseDouble(horse.get(0));
                double lon2 = Double.parseDouble(horse.get(1));
                double dist = UniqueCordinates.rangeCalculation(lat, lon, lat2, lon2);
//                            out.println(i+" >>>"+dist);
                out.println("@@@@@@@@@@@@@@@@ " + cor + " to " + sf + " ---> " + dist + " @@@@@@@@@@@@@@@@@@@@@");
                temp_Value = dist;
                temp_String = sf;

                if (dist > 0.115) {
                    flag = 1;
                    if (temp_Value < initial_Value) {
                        initial_Value = temp_Value;
                        initial_String = temp_String;
                    }
                }

            }

        } //****************************************************************************           
        else if (time == 5) {
            String temp_String = "";
            double temp_Value = 0.0;

//            double store[] = new double[PredictiveLocationGeneration.one.size() + 1];
            for (int i = 0; i < PredictiveLocationGeneration.six.size(); i++) {

                String sf = PredictiveLocationGeneration.six.get(i).toString();
                List<String> horse = Arrays.asList(sf.split(", "));

                double lat2 = Double.parseDouble(horse.get(0));
                double lon2 = Double.parseDouble(horse.get(1));
                double dist = UniqueCordinates.rangeCalculation(lat, lon, lat2, lon2);
//                            out.println(i+" >>>"+dist);
                out.println("@@@@@@@@@@@@@@@@ " + cor + " to " + sf + " ---> " + dist + " @@@@@@@@@@@@@@@@@@@@@");
                temp_Value = dist;
                temp_String = sf;

                if (dist > 0.115) {
                    flag = 1;
                    if (temp_Value < initial_Value) {
                        initial_Value = temp_Value;
                        initial_String = temp_String;
                    }
                }

            }

        } //*************************************************************************         
        else if (time == 6) {

            String temp_String = "";
            double temp_Value = 0.0;

//            double store[] = new double[PredictiveLocationGeneration.one.size() + 1];
            for (int i = 0; i < PredictiveLocationGeneration.seven.size(); i++) {

                String sf = PredictiveLocationGeneration.seven.get(i).toString();
                List<String> horse = Arrays.asList(sf.split(", "));

                double lat2 = Double.parseDouble(horse.get(0));
                double lon2 = Double.parseDouble(horse.get(1));
                double dist = UniqueCordinates.rangeCalculation(lat, lon, lat2, lon2);
//                            out.println(i+" >>>"+dist);
                out.println("@@@@@@@@@@@@@@@@ " + cor + " to " + sf + " ---> " + dist + " @@@@@@@@@@@@@@@@@@@@@");
                temp_Value = dist;
                temp_String = sf;

                if (dist > 0.115) {
                    flag = 1;
                    if (temp_Value < initial_Value) {
                        initial_Value = temp_Value;
                        initial_String = temp_String;
                    }
                }

            }

        } //*************************************************************************        
        else if (time == 7) {
            String temp_String = "";
            double temp_Value = 0.0;

//            double store[] = new double[PredictiveLocationGeneration.one.size() + 1];
            for (int i = 0; i < PredictiveLocationGeneration.eight.size(); i++) {

                String sf = PredictiveLocationGeneration.eight.get(i).toString();
                List<String> horse = Arrays.asList(sf.split(", "));

                double lat2 = Double.parseDouble(horse.get(0));
                double lon2 = Double.parseDouble(horse.get(1));
                double dist = UniqueCordinates.rangeCalculation(lat, lon, lat2, lon2);
//                            out.println(i+" >>>"+dist);
                out.println("@@@@@@@@@@@@@@@@ " + cor + " to " + sf + " ---> " + dist + " @@@@@@@@@@@@@@@@@@@@@");
                temp_Value = dist;
                temp_String = sf;

                if (dist > 0.115) {
                    flag = 1;
                    if (temp_Value < initial_Value) {
                        initial_Value = temp_Value;
                        initial_String = temp_String;
                    }
                }

            }

        } //**************************************************************************        
        else if (time == 8) {

            String temp_String = "";
            double temp_Value = 0.0;

//            double store[] = new double[PredictiveLocationGeneration.one.size() + 1];
            for (int i = 0; i < PredictiveLocationGeneration.nine.size(); i++) {

                String sf = PredictiveLocationGeneration.nine.get(i).toString();
                List<String> horse = Arrays.asList(sf.split(", "));

                double lat2 = Double.parseDouble(horse.get(0));
                double lon2 = Double.parseDouble(horse.get(1));
                double dist = UniqueCordinates.rangeCalculation(lat, lon, lat2, lon2);
//                            out.println(i+" >>>"+dist);
                out.println("@@@@@@@@@@@@@@@@ " + cor + " to " + sf + " ---> " + dist + " @@@@@@@@@@@@@@@@@@@@@");
                temp_Value = dist;
                temp_String = sf;

                if (dist > 0.115) {
                    flag = 1;
                    if (temp_Value < initial_Value) {
                        initial_Value = temp_Value;
                        initial_String = temp_String;
                    }
                }

            }

        } //**************************************************************************        
        else if (time == 9) {

            String temp_String = "";
            double temp_Value = 0.0;

//            double store[] = new double[PredictiveLocationGeneration.one.size() + 1];
            for (int i = 0; i < PredictiveLocationGeneration.ten.size(); i++) {

                String sf = PredictiveLocationGeneration.ten.get(i).toString();
                List<String> horse = Arrays.asList(sf.split(", "));

                double lat2 = Double.parseDouble(horse.get(0));
                double lon2 = Double.parseDouble(horse.get(1));
                double dist = UniqueCordinates.rangeCalculation(lat, lon, lat2, lon2);
//                            out.println(i+" >>>"+dist);
                out.println("@@@@@@@@@@@@@@@@ " + cor + " to " + sf + " ---> " + dist + " @@@@@@@@@@@@@@@@@@@@@");
                temp_Value = dist;
                temp_String = sf;

                if (dist > 0.115) {
                    flag = 1;
                    if (temp_Value < initial_Value) {
                        initial_Value = temp_Value;
                        initial_String = temp_String;
                    }
                }

            }

        } //***************************************************************************
        else if (time == 10) {

            String temp_String = "";
            double temp_Value = 0.0;

//            double store[] = new double[PredictiveLocationGeneration.one.size() + 1];
            for (int i = 0; i < PredictiveLocationGeneration.eleven.size(); i++) {

                String sf = PredictiveLocationGeneration.eleven.get(i).toString();
                List<String> horse = Arrays.asList(sf.split(", "));

                double lat2 = Double.parseDouble(horse.get(0));
                double lon2 = Double.parseDouble(horse.get(1));
                double dist = UniqueCordinates.rangeCalculation(lat, lon, lat2, lon2);
//                            out.println(i+" >>>"+dist);
                out.println("@@@@@@@@@@@@@@@@ " + cor + " to " + sf + " ---> " + dist + " @@@@@@@@@@@@@@@@@@@@@");
                temp_Value = dist;
                temp_String = sf;

                if (dist > 0.115) {
                    flag = 1;
                    if (temp_Value < initial_Value) {
                        initial_Value = temp_Value;
                        initial_String = temp_String;
                    }
                }

            }

        } //*******************************************************************************     
        else if (time == 11) {

            String temp_String = "";
            double temp_Value = 0.0;

//            double store[] = new double[PredictiveLocationGeneration.one.size() + 1];
            for (int i = 0; i < PredictiveLocationGeneration.twelve.size(); i++) {

                String sf = PredictiveLocationGeneration.twelve.get(i).toString();
                List<String> horse = Arrays.asList(sf.split(", "));

                double lat2 = Double.parseDouble(horse.get(0));
                double lon2 = Double.parseDouble(horse.get(1));
                double dist = UniqueCordinates.rangeCalculation(lat, lon, lat2, lon2);
//                            out.println(i+" >>>"+dist);
                out.println("@@@@@@@@@@@@@@@@ " + cor + " to " + sf + " ---> " + dist + " @@@@@@@@@@@@@@@@@@@@@");
                temp_Value = dist;
                temp_String = sf;

                if (dist > 0.115) {
                    flag = 1;
                    if (temp_Value < initial_Value) {
                        initial_Value = temp_Value;
                        initial_String = temp_String;
                    }
                }

            }

        } //****************************************************************************************        
        else if (time == 12) {

            String temp_String = "";
            double temp_Value = 0.0;

//            double store[] = new double[PredictiveLocationGeneration.one.size() + 1];
            for (int i = 0; i < PredictiveLocationGeneration.thirteen.size(); i++) {

                String sf = PredictiveLocationGeneration.thirteen.get(i).toString();
                List<String> horse = Arrays.asList(sf.split(", "));

                double lat2 = Double.parseDouble(horse.get(0));
                double lon2 = Double.parseDouble(horse.get(1));
                double dist = UniqueCordinates.rangeCalculation(lat, lon, lat2, lon2);
//                            out.println(i+" >>>"+dist);
                out.println("@@@@@@@@@@@@@@@@ " + cor + " to " + sf + " ---> " + dist + " @@@@@@@@@@@@@@@@@@@@@");
                temp_Value = dist;
                temp_String = sf;

                if (dist > 0.115) {
                    flag = 1;
                    if (temp_Value < initial_Value) {
                        initial_Value = temp_Value;
                        initial_String = temp_String;
                    }
                }

            }

        } //****************************************************************************************    
        else if (time == 13) {

            String temp_String = "";
            double temp_Value = 0.0;

//            double store[] = new double[PredictiveLocationGeneration.one.size() + 1];
            for (int i = 0; i < PredictiveLocationGeneration.fourteen.size(); i++) {

                String sf = PredictiveLocationGeneration.fourteen.get(i).toString();
                List<String> horse = Arrays.asList(sf.split(", "));

                double lat2 = Double.parseDouble(horse.get(0));
                double lon2 = Double.parseDouble(horse.get(1));
                double dist = UniqueCordinates.rangeCalculation(lat, lon, lat2, lon2);
//                            out.println(i+" >>>"+dist);
                out.println("@@@@@@@@@@@@@@@@ " + cor + " to " + sf + " ---> " + dist + " @@@@@@@@@@@@@@@@@@@@@");
                temp_Value = dist;
                temp_String = sf;

                if (dist > 0.115) {
                    flag = 1;
                    if (temp_Value < initial_Value) {
                        initial_Value = temp_Value;
                        initial_String = temp_String;
                    }
                }

            }

        } //****************************************************************************************       
        else if (time == 14) {

            String temp_String = "";
            double temp_Value = 0.0;

//            double store[] = new double[PredictiveLocationGeneration.one.size() + 1];
            for (int i = 0; i < PredictiveLocationGeneration.fifteen.size(); i++) {

                String sf = PredictiveLocationGeneration.fifteen.get(i).toString();
                List<String> horse = Arrays.asList(sf.split(", "));

                double lat2 = Double.parseDouble(horse.get(0));
                double lon2 = Double.parseDouble(horse.get(1));
                double dist = UniqueCordinates.rangeCalculation(lat, lon, lat2, lon2);
//                            out.println(i+" >>>"+dist);
                out.println("@@@@@@@@@@@@@@@@ " + cor + " to " + sf + " ---> " + dist + " @@@@@@@@@@@@@@@@@@@@@");
                temp_Value = dist;
                temp_String = sf;

                if (dist > 0.115) {
                    flag = 1;
                    if (temp_Value < initial_Value) {
                        initial_Value = temp_Value;
                        initial_String = temp_String;
                    }
                }

            }

        } //****************************************************************************************
        else if (time == 15) {
            String temp_String = "";
            double temp_Value = 0.0;

//            double store[] = new double[PredictiveLocationGeneration.one.size() + 1];
            for (int i = 0; i < PredictiveLocationGeneration.sixteen.size(); i++) {

                String sf = PredictiveLocationGeneration.sixteen.get(i).toString();
                List<String> horse = Arrays.asList(sf.split(", "));

                double lat2 = Double.parseDouble(horse.get(0));
                double lon2 = Double.parseDouble(horse.get(1));
                double dist = UniqueCordinates.rangeCalculation(lat, lon, lat2, lon2);
//                            out.println(i+" >>>"+dist);
                out.println("@@@@@@@@@@@@@@@@ " + cor + " to " + sf + " ---> " + dist + " @@@@@@@@@@@@@@@@@@@@@");
                temp_Value = dist;
                temp_String = sf;

                if (dist > 0.115) {
                    flag = 1;
                    if (temp_Value < initial_Value) {
                        initial_Value = temp_Value;
                        initial_String = temp_String;
                    }
                }

            }

        } //****************************************************************************************
        else if (time == 16) {

            String temp_String = "";
            double temp_Value = 0.0;

//            double store[] = new double[PredictiveLocationGeneration.one.size() + 1];
            for (int i = 0; i < PredictiveLocationGeneration.seventeen.size(); i++) {

                String sf = PredictiveLocationGeneration.seventeen.get(i).toString();
                List<String> horse = Arrays.asList(sf.split(", "));

                double lat2 = Double.parseDouble(horse.get(0));
                double lon2 = Double.parseDouble(horse.get(1));
                double dist = UniqueCordinates.rangeCalculation(lat, lon, lat2, lon2);
//                            out.println(i+" >>>"+dist);
                out.println("@@@@@@@@@@@@@@@@ " + cor + " to " + sf + " ---> " + dist + " @@@@@@@@@@@@@@@@@@@@@");
                temp_Value = dist;
                temp_String = sf;

                if (dist > 0.115) {
                    flag = 1;
                    if (temp_Value < initial_Value) {
                        initial_Value = temp_Value;
                        initial_String = temp_String;
                    }
                }

            }

        } //****************************************************************************************
        else if (time == 17) {
            String temp_String = "";
            double temp_Value = 0.0;

//            double store[] = new double[PredictiveLocationGeneration.one.size() + 1];
            for (int i = 0; i < PredictiveLocationGeneration.eighteen.size(); i++) {

                String sf = PredictiveLocationGeneration.eighteen.get(i).toString();
                List<String> horse = Arrays.asList(sf.split(", "));

                double lat2 = Double.parseDouble(horse.get(0));
                double lon2 = Double.parseDouble(horse.get(1));
                double dist = UniqueCordinates.rangeCalculation(lat, lon, lat2, lon2);
//                            out.println(i+" >>>"+dist);
                out.println("@@@@@@@@@@@@@@@@ " + cor + " to " + sf + " ---> " + dist + " @@@@@@@@@@@@@@@@@@@@@");
                temp_Value = dist;
                temp_String = sf;

                if (dist > 0.115) {
                    flag = 1;
                    if (temp_Value < initial_Value) {
                        initial_Value = temp_Value;
                        initial_String = temp_String;
                    }
                }

            }

        } //***************************************************************************
        else if (time == 18) {

            String temp_String = "";
            double temp_Value = 0.0;

//            double store[] = new double[PredictiveLocationGeneration.one.size() + 1];
            for (int i = 0; i < PredictiveLocationGeneration.nineteen.size(); i++) {

                String sf = PredictiveLocationGeneration.nineteen.get(i).toString();
                List<String> horse = Arrays.asList(sf.split(", "));

                double lat2 = Double.parseDouble(horse.get(0));
                double lon2 = Double.parseDouble(horse.get(1));
                double dist = UniqueCordinates.rangeCalculation(lat, lon, lat2, lon2);
//                            out.println(i+" >>>"+dist);
                out.println("@@@@@@@@@@@@@@@@ " + cor + " to " + sf + " ---> " + dist + " @@@@@@@@@@@@@@@@@@@@@");
                temp_Value = dist;
                temp_String = sf;

                if (dist > 0.115) {
                    flag = 1;
                    if (temp_Value < initial_Value) {
                        initial_Value = temp_Value;
                        initial_String = temp_String;
                    }
                }

            }

        } //***************************************************************************
        else if (time == 19) {

            String temp_String = "";
            double temp_Value = 0.0;

//            double store[] = new double[PredictiveLocationGeneration.one.size() + 1];
            for (int i = 0; i < PredictiveLocationGeneration.twenty.size(); i++) {

                String sf = PredictiveLocationGeneration.twenty.get(i).toString();
                List<String> horse = Arrays.asList(sf.split(", "));

                double lat2 = Double.parseDouble(horse.get(0));
                double lon2 = Double.parseDouble(horse.get(1));
                double dist = UniqueCordinates.rangeCalculation(lat, lon, lat2, lon2);
//                            out.println(i+" >>>"+dist);
                out.println("@@@@@@@@@@@@@@@@ " + cor + " to " + sf + " ---> " + dist + " @@@@@@@@@@@@@@@@@@@@@");
                temp_Value = dist;
                temp_String = sf;

                if (dist > 0.115) {
                    flag = 1;
                    if (temp_Value < initial_Value) {
                        initial_Value = temp_Value;
                        initial_String = temp_String;
                    }
                }

            }

        } //***************************************************************************
        else if (time == 20) {

            String temp_String = "";
            double temp_Value = 0.0;

//            double store[] = new double[PredictiveLocationGeneration.one.size() + 1];
            for (int i = 0; i < PredictiveLocationGeneration.twentyone.size(); i++) {

                String sf = PredictiveLocationGeneration.twentyone.get(i).toString();
                List<String> horse = Arrays.asList(sf.split(", "));

                double lat2 = Double.parseDouble(horse.get(0));
                double lon2 = Double.parseDouble(horse.get(1));
                double dist = UniqueCordinates.rangeCalculation(lat, lon, lat2, lon2);
//                            out.println(i+" >>>"+dist);
                out.println("@@@@@@@@@@@@@@@@ " + cor + " to " + sf + " ---> " + dist + " @@@@@@@@@@@@@@@@@@@@@");
                temp_Value = dist;
                temp_String = sf;

                if (dist > 0.115) {
                    flag = 1;
                    if (temp_Value < initial_Value) {
                        initial_Value = temp_Value;
                        initial_String = temp_String;
                    }
                }

            }

        } //****************************************************************************************
        else if (time == 21) {

            String temp_String = "";
            double temp_Value = 0.0;

//            double store[] = new double[PredictiveLocationGeneration.one.size() + 1];
            for (int i = 0; i < PredictiveLocationGeneration.twentytwo.size(); i++) {

                String sf = PredictiveLocationGeneration.twentytwo.get(i).toString();
                List<String> horse = Arrays.asList(sf.split(", "));

                double lat2 = Double.parseDouble(horse.get(0));
                double lon2 = Double.parseDouble(horse.get(1));
                double dist = UniqueCordinates.rangeCalculation(lat, lon, lat2, lon2);
//                            out.println(i+" >>>"+dist);
                out.println("@@@@@@@@@@@@@@@@ " + cor + " to " + sf + " ---> " + dist + " @@@@@@@@@@@@@@@@@@@@@");
                temp_Value = dist;
                temp_String = sf;

                if (dist > 0.115) {
                    flag = 1;
                    if (temp_Value < initial_Value) {
                        initial_Value = temp_Value;
                        initial_String = temp_String;
                    }
                }

            }

        } //****************************************************************************************
        else if (time == 22) {

            String temp_String = "";
            double temp_Value = 0.0;

//            double store[] = new double[PredictiveLocationGeneration.one.size() + 1];
            for (int i = 0; i < PredictiveLocationGeneration.twentythree.size(); i++) {

                String sf = PredictiveLocationGeneration.twentythree.get(i).toString();
                List<String> horse = Arrays.asList(sf.split(", "));

                double lat2 = Double.parseDouble(horse.get(0));
                double lon2 = Double.parseDouble(horse.get(1));
                double dist = UniqueCordinates.rangeCalculation(lat, lon, lat2, lon2);
//                            out.println(i+" >>>"+dist);
                out.println("@@@@@@@@@@@@@@@@ " + cor + " to " + sf + " ---> " + dist + " @@@@@@@@@@@@@@@@@@@@@");
                temp_Value = dist;
                temp_String = sf;

                if (dist > 0.115) {
                    flag = 1;
                    if (temp_Value < initial_Value) {
                        initial_Value = temp_Value;
                        initial_String = temp_String;
                    }
                }

            }

        } //****************************************************************************************
        else if (time == 23) {

            String temp_String = "";
            double temp_Value = 0.0;

//            double store[] = new double[PredictiveLocationGeneration.one.size() + 1];
            for (int i = 0; i < PredictiveLocationGeneration.zero.size(); i++) {

                String sf = PredictiveLocationGeneration.zero.get(i).toString();
                List<String> horse = Arrays.asList(sf.split(", "));

                double lat2 = Double.parseDouble(horse.get(0));
                double lon2 = Double.parseDouble(horse.get(1));
                double dist = UniqueCordinates.rangeCalculation(lat, lon, lat2, lon2);
//                            out.println(i+" >>>"+dist);
                out.println("@@@@@@@@@@@@@@@@ " + cor + " to " + sf + " ---> " + dist + " @@@@@@@@@@@@@@@@@@@@@");
                temp_Value = dist;
                temp_String = sf;

                if (dist > 0.115) {
                    flag = 1;
                    if (temp_Value < initial_Value) {
                        initial_Value = temp_Value;
                        initial_String = temp_String;
                    }
                }

            }

        }
//*****************************************************************************************

        if (flag != 1) {
            return ("Predicted Location For << " + cor + " >> is " + "( " + cor + " )" + " at " + time + ":00");
        }
        return ("Predicted Location For <<" + cor + " >> is " + "( " + initial_String + " )" + " at " + time + ":00");
    }
    //############################################################################

    public static String searching_child(String alName, double lat, double lon) {

        return "";
    }

    //############################################################################
}
