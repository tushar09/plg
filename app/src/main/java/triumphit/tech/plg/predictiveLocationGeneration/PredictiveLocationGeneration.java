/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triumphit.tech.plg.predictiveLocationGeneration;

import java.util.ArrayList;
import java.util.*;
import java.lang.*;
import java.io.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import static java.lang.System.out;

/**
 *
 * @author khaled
 */
public class PredictiveLocationGeneration {

    static int keep_track = 0;
    static int signal_flag = 0;
    public static LinkedList[] cor_time = new LinkedList[23];
    public static ArrayList zero = new ArrayList();
    public static ArrayList one = new ArrayList();
    public static ArrayList two = new ArrayList();
    public static ArrayList three = new ArrayList();
    public static ArrayList four = new ArrayList();
    public static ArrayList five = new ArrayList();
    public static ArrayList six = new ArrayList();
    public static ArrayList seven = new ArrayList();
    public static ArrayList eight = new ArrayList();
    public static ArrayList nine = new ArrayList();
    public static ArrayList ten = new ArrayList();
    public static ArrayList eleven = new ArrayList();
    public static ArrayList twelve = new ArrayList();
    public static ArrayList thirteen = new ArrayList();
    public static ArrayList fourteen = new ArrayList();
    public static ArrayList fifteen = new ArrayList();
    public static ArrayList sixteen = new ArrayList();
    public static ArrayList seventeen = new ArrayList();
    public static ArrayList eighteen = new ArrayList();
    public static ArrayList nineteen = new ArrayList();
    public static ArrayList twenty = new ArrayList();
    public static ArrayList twentyone = new ArrayList();
    public static ArrayList twentytwo = new ArrayList();
    public static ArrayList twentythree = new ArrayList();
    //###############################################################################

    PredictiveLocationGeneration() {}
    
    
    //###############################################################################
    PredictiveLocationGeneration(int i) {
        Scanner k = new Scanner(System.in);
        out.println("***********************");
        out.println("Please enter latitude");
        double lat = k.nextDouble();
        out.println("***********************");
        out.println("Please enter longitude");
        double lon = k.nextDouble();
        out.println("***********************");
        out.println("Please enter time");
        int time = k.nextInt();
        out.println("***********************");
        int whl = 0;
        while (whl == 0) {
            if (time > -1 && time < 24) {
                new PredictiveLocationGeneration(time, lat, lon);
                whl = 1;
            } else {
                out.println("***********************");
                out.println("Please enter the time");
                time = k.nextInt();
                out.println("***********************");

            }
        }
    }

    //###################################################################################  
    PredictiveLocationGeneration(int time, double lat, double lon) {

        UniqueCordinates.check_Add_NewUnique_Location(lat, lon);

        if (signal_flag == 1) {
            signal_flag = 0;
            out.println("This is a new location so no predicted locations corresponding to this corodinate is available.");

        } else {
            String predicted_Locataion=PredictionBasement.make_Prediction(time, lat, lon);
            out.println();
            out.println("#################################################################");
            out.println(predicted_Locataion);
            out.println();        
        }

    }

    //##############################################################################
    public static void main(String[] args) {
//        UniqueCordinates un = new UniqueCordinates();
//          PredictionBasement p=new PredictionBasement();

        DataClassification dt = new DataClassification();
        out.println("###########################<< Decision Tree's Nodes and leaf Nodes >>####################################");
        out.println("Time==0:00: " + zero);
        out.println("Time==01:00: " + one);
        out.println("Time==02:00: " + two);
        out.println("Time==03:00: " + three);
        out.println("Time==04:00: " + four);
        out.println("Time==05:00: " + five);
        out.println("Time==06:00: " + six);
        out.println("Time==07:00: " + seven);
        out.println("Time==08:00: " + eight);
        out.println("Time==09:00: " + nine);
        out.println("Time==10:00: " + ten);
        out.println("Time==11:00: " + eleven);
        out.println("Time==12:00: " + twelve);
        out.println("Time==13:00: " + thirteen);
        out.println("Time==14:00: " + fourteen);
        out.println("Time==15:00: " + fifteen);
        out.println("Time==16:00: " + sixteen);
        out.println("Time==17:00: " + seventeen);
        out.println("Time==18:00: " + eighteen);
        out.println("Time==19:00: " + nineteen);
        out.println("Time==20:00: " + twenty);
        out.println("Time==21:00: " + twentyone);
        out.println("Time==22:00: " + twentytwo);
        out.println("Time==23:00: " + twentythree);
        out.println("###############################################################");

        new PredictiveLocationGeneration(100);

    }
//##################################################################################
}
