/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triumphit.tech.plg.predictiveLocationGeneration;

/**
 *
 * @author khaled
 *
 *
 */
import java.util.*;
import java.lang.*;
import java.io.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import static java.lang.System.out;

//########################################################################################
public class UniqueCordinates {

    static double[] time = new double[23];
    static double lat1;
    static double lon1;
    private static final String FILENAME = "D:\\Netbeans\\DecisionTree\\PredictiveLocationGeneration\\CordinatesforApp.txt";
    static ArrayList aList = new ArrayList();
//########################################################################################

     UniqueCordinates() {

        double f = this.rangeCalculation(23.870263, 90.405461, 23.780780, 90.407300);
        out.println("Testing the measured distance: " + f + " Kilometer--for--Value--(23.870263, 90.405461, 23.780780, 90.407300)");
        this.updateUniqueLocation();
        check_Add_NewUnique_Location(23.864358, 90.399913);
        check_Add_NewUnique_Location(23.864358, 90.399924);
//        check_Add_NewUnique_Location(24.864359, 91.399929);
        
    }
    
    //#################################################################################
    
    UniqueCordinates(int i){
   
    
    }
    
    //#################################################################################
    

//**********************************************************************************    
    public static void updateUniqueLocation() {
        int c_time=0;
        try (BufferedReader br = new BufferedReader(new FileReader(FILENAME))) {

            String sCurrentLine;

//            String s = "23.123456789";
//
//            double d = Double.parseDouble(s);
//            System.out.println(d);
            while ((sCurrentLine = br.readLine()) != null) {
                int flag = 0;
                int redFlag = 0;
                String str = sCurrentLine;
                if (!str.equals("")) {
//               
                    if (aList.contains(str)) {

                    } else if (aList.size() == 0) {

                        aList.add(str);

                    } else {

                        List<String> elephantList = Arrays.asList(str.split(", "));
                        lat1 = Double.parseDouble(elephantList.get(0));
                        lon1 = Double.parseDouble(elephantList.get(1));

                        for (int i = 0; i < aList.size(); i++) {

                            String sf = aList.get(i).toString();
                            List<String> horse = Arrays.asList(sf.split(", "));
                            double lat2 = Double.parseDouble(horse.get(0));
                            double lon2 = Double.parseDouble(horse.get(1));
                            double dist = rangeCalculation(lat1, lon1, lat2, lon2);
//                            out.println(i+" >>>"+dist);
                            if (dist < 0.155) {
                                redFlag = 1;
                                i = aList.size();
                            }

                        }

                        if (redFlag == 0) {

                            aList.add(str);
                        }
                    }

//               
                }//if
//				System.out.println(sCurrentLine);
//           c_time++;
            }//while

            //checking that the objects are stored or not.
            System.out.println("##############################################");
            System.out.println("Unique Cordinate's ArrayList: ");
            printArrayList();

        } catch (IOException e) {
            e.printStackTrace();
        }
      PredictiveLocationGeneration.keep_track= c_time;
    }

//************************************************************************************
    public static double rangeCalculation(double lat1_deg, double lon1_deg, double lat2_deg, double lon2_deg) {
        double earthRadius = 6371; // in kilometers

        double lat1 = Math.toRadians(lat1_deg);
        double lon1 = Math.toRadians(lon1_deg);
        double lat2 = Math.toRadians(lat2_deg);
        double lon2 = Math.toRadians(lon2_deg);

        double dlon = Math.abs(lon2 - lon1);
        double dlat = Math.abs(lat2 - lat1);

        double a = (Math.sin(dlat / 2)) * (Math.sin(dlat / 2))
                + (Math.cos(lat1) * Math.cos(lat2) * (Math.sin(dlon / 2)))
                * (Math.cos(lat1) * Math.cos(lat2) * (Math.sin(dlon / 2)));

        double c = 2 * Math.asin(Math.min(1.0, Math.sqrt(a)));
        double km = earthRadius * c;

        return km;

    }

    //################################################################################
    public static void check_Add_NewUnique_Location(double lat1, double lon1) {

        int flag = 0;
        String cor = "" + lat1 + ", " + lon1;

        if (!aList.contains(cor)) {
            for (int i = 0; i < aList.size(); i++) {

                String sf = aList.get(i).toString();
                List<String> horse = Arrays.asList(sf.split(", "));

                double lat2 = Double.parseDouble(horse.get(0));
                double lon2 = Double.parseDouble(horse.get(1));
                double dist = rangeCalculation(lat1, lon1, lat2, lon2);
//                            out.println(i+" >>>"+dist);

                if (dist < 0.115) {
                    flag = 1;
                    out.println("##################################################");
                    out.println("<< "+cor+" >>"+"This cordinate is in range of " + lat2 + ", " + lon2);
                    i = aList.size();
                
                }

            }
            if (flag != 1) {
                PredictiveLocationGeneration.signal_flag=1;
                aList.add(cor);
                out.println("####################################################");
                out.println("**********New ArrayList after adding<< "+cor+" >>*************");
                out.println("####################################################");
                printArrayList();
            }
        }
        else{
            out.println("##########################################################");
            out.println("<< "+cor+" >>This cordinate already exists");
            }
        

    }

//######################################################################################
    public static void printArrayList() {

        for (int i = 0; i < aList.size(); i++) {

            String sf = aList.get(i).toString();

            System.out.println(i + " == " + sf);
        }

    }

//#########################################################################################   

    
    
    
    //######################################################################################
    
    
}
