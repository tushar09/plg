package triumphit.tech.plg.db;

import org.greenrobot.greendao.annotation.*;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END
/**
 * Entity mapped to table "PREDICTED_LOCATION".
 */
@Entity
public class PredictedLocation {

    @Id(autoincrement = true)
    @Unique
    private Long id;
    private Integer time;
    private String MONDAY;
    private String TUESDAY;
    private String WEDNESDAY;
    private String THURSDAY;
    private String FRIDAY;
    private String SATERDAY;
    private String SUNDAY;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    @Generated
    public PredictedLocation() {
    }

    public PredictedLocation(Long id) {
        this.id = id;
    }

    @Generated
    public PredictedLocation(Long id, Integer time, String MONDAY, String TUESDAY, String WEDNESDAY, String THURSDAY, String FRIDAY, String SATERDAY, String SUNDAY) {
        this.id = id;
        this.time = time;
        this.MONDAY = MONDAY;
        this.TUESDAY = TUESDAY;
        this.WEDNESDAY = WEDNESDAY;
        this.THURSDAY = THURSDAY;
        this.FRIDAY = FRIDAY;
        this.SATERDAY = SATERDAY;
        this.SUNDAY = SUNDAY;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getMONDAY() {
        return MONDAY;
    }

    public void setMONDAY(String MONDAY) {
        this.MONDAY = MONDAY;
    }

    public String getTUESDAY() {
        return TUESDAY;
    }

    public void setTUESDAY(String TUESDAY) {
        this.TUESDAY = TUESDAY;
    }

    public String getWEDNESDAY() {
        return WEDNESDAY;
    }

    public void setWEDNESDAY(String WEDNESDAY) {
        this.WEDNESDAY = WEDNESDAY;
    }

    public String getTHURSDAY() {
        return THURSDAY;
    }

    public void setTHURSDAY(String THURSDAY) {
        this.THURSDAY = THURSDAY;
    }

    public String getFRIDAY() {
        return FRIDAY;
    }

    public void setFRIDAY(String FRIDAY) {
        this.FRIDAY = FRIDAY;
    }

    public String getSATERDAY() {
        return SATERDAY;
    }

    public void setSATERDAY(String SATERDAY) {
        this.SATERDAY = SATERDAY;
    }

    public String getSUNDAY() {
        return SUNDAY;
    }

    public void setSUNDAY(String SUNDAY) {
        this.SUNDAY = SUNDAY;
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
