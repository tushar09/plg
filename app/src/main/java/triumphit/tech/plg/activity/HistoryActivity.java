package triumphit.tech.plg.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import triumphit.tech.plg.R;
import triumphit.tech.plg.adapters.HistoryAdapters;
import triumphit.tech.plg.adapters.InternalDataAdapters;
import triumphit.tech.plg.db.PredictedLocation;
import triumphit.tech.plg.db.PredictedLocationHistory;
import triumphit.tech.plg.db.UniqLocation;
import triumphit.tech.plg.utils.DBHepler;
import triumphit.tech.plg.utils.Extractor;

public class HistoryActivity extends AppCompatActivity{

    ListView lv;
    DBHepler dbHepler;
    List<Extractor> pl;
    List<UniqLocation> ul;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        lv = (ListView) findViewById(R.id.lv_list);
        dbHepler = new DBHepler(this);

        if(getIntent().getStringExtra("task").equals("history")){
            getSupportActionBar().setTitle("History");
            List<PredictedLocationHistory> locations = dbHepler.getPredectedLocationHistoryAsList();
            Collections.reverse(locations);
//            List<PredictedLocation> locations = dbHepler.getPredectLocationAsList();
//            pl = new ArrayList<>();
//
//            for(int t = 0; t < locations.size(); t++){
//                Extractor p = new Extractor();
//                p.setTime(locations.get(t).getTime() + "");
//                p.setLatlng(locations.get(t).getMONDAY());
//                p.setDayName("Monday");
//                pl.add(p);
//            }
//            for(int t = 0; t < locations.size(); t++){
//                Extractor p = new Extractor();
//                p.setTime(locations.get(t).getTime() + "");
//                p.setLatlng(locations.get(t).getTUESDAY());
//                p.setDayName("Tuesday");
//                pl.add(p);
//            }
//            for(int t = 0; t < locations.size(); t++){
//                Extractor p = new Extractor();
//                p.setTime(locations.get(t).getTime() + "");
//                p.setLatlng(locations.get(t).getWEDNESDAY());
//                p.setDayName("Wednesday");
//                pl.add(p);
//            }
//            for(int t = 0; t < locations.size(); t++){
//                Extractor p = new Extractor();
//                p.setTime(locations.get(t).getTime() + "");
//                p.setLatlng(locations.get(t).getTHURSDAY());
//                p.setDayName("Thursday");
//                pl.add(p);
//            }
//            for(int t = 0; t < locations.size(); t++){
//                Extractor p = new Extractor();
//                p.setTime(locations.get(t).getTime() + "");
//                p.setLatlng(locations.get(t).getFRIDAY());
//                p.setDayName("Friday");
//                pl.add(p);
//            }
//            for(int t = 0; t < locations.size(); t++){
//                Extractor p = new Extractor();
//                p.setTime(locations.get(t).getTime() + "");
//                p.setLatlng(locations.get(t).getSATERDAY());
//                p.setDayName("Saterday");
//                pl.add(p);
//            }
//            for(int t = 0; t < locations.size(); t++){
//                Extractor p = new Extractor();
//                p.setTime(locations.get(t).getTime() + "");
//                p.setLatlng(locations.get(t).getSUNDAY());
//                p.setDayName("Sunday");
//                pl.add(p);
//            }

            lv.setAdapter(new HistoryAdapters(this, locations, ""));
        }else {
            ul = dbHepler.getLocatioAsList();
            getSupportActionBar().setTitle("Internal DB: " + ul.size() + " Items");
            //Collections.reverse(ul);
            lv.setAdapter(new InternalDataAdapters(this, ul));
        }


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

}
