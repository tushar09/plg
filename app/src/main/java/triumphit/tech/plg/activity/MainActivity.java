package triumphit.tech.plg.activity;

import android.Manifest;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import triumphit.tech.plg.R;
import triumphit.tech.plg.databinding.ActivityMainBinding;
import triumphit.tech.plg.db.PredictedLocation;
import triumphit.tech.plg.db.PredictedLocationHistory;
import triumphit.tech.plg.db.UniqLocation;
import triumphit.tech.plg.utils.Constants;
import triumphit.tech.plg.utils.DBHepler;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private ActivityMainBinding binding;
    private boolean IS_MAP_READY;
    private GoogleMap googleMap;
    private GoogleApiClient mGoogleApiClient;
    private Location location;
    private LocationRequest mLocationRequest;
    private double currentLatitude;
    private double currentLongitude;
    private double distanceMeasureLaitude;
    private double distanceMeasureLongitude;
    private DBHepler dbHepler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dbHepler = new DBHepler(this);

        if(getIntent().getBooleanExtra("hide", false) == true){
            binding.container.llMap.setVisibility(View.GONE);
            binding.container.cvInfoHolder.setVisibility(View.VISIBLE);
        }else {
            binding.container.llMap.setVisibility(View.VISIBLE);
            binding.container.cvInfoHolder.setVisibility(View.GONE);
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                // The next two lines tell the new client that “this” current class will handle connection stuff
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                //fourth line adds the LocationServices API endpoint from GooglePlayServices
                .addApi(LocationServices.API)
                .build();
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Constants.initPreUniqLocation(this);
        Constants.initPredictedLocation(this);

        Log.e("asdf", dbHepler.getLocatioAsList().size() + "");


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.REQUEST_ACCESSFINELOCATION);
            return;
        }
        location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } else {
            location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            readLocation(location);
        }
    }

    private void readLocation(Location location) {
        //If everything went fine lets get latitude and longitude
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
        //loadHome(currentLatitude, currentLongitude);
        Log.e("Error", "" + currentLatitude + " " + currentLongitude);
        //Toast.makeText(this, currentLatitude + " WORKS " + currentLongitude + "", Toast.LENGTH_LONG).show();

        if (IS_MAP_READY) {
            LatLng current = new LatLng(currentLatitude, currentLongitude);
            googleMap.addMarker(new MarkerOptions().position(current)
                    .title("My Current Location").icon(BitmapDescriptorFactory.defaultMarker(120)));
            distanceMeasureLaitude = googleMap.getCameraPosition().target.latitude;
            distanceMeasureLongitude = googleMap.getCameraPosition().target.longitude;
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(current)
                    .zoom(Constants.MAP_ZOOM_LEVEL)   // Sets the zoom level
                    .bearing(0)                       // Sets the orientation of the camera to east
                    .tilt(30)                           // Sets the tilt of the camera to 30 degrees
                    .build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            Double lat = 0.0;
            Double lng = 0.0;
            String name = "";
            Float shortest = 1001.0F;

            for (int t = 0; t < Constants.locationGeo.size(); t++) {
                String latLng = Constants.locationGeo.get(t);
                String[] latLngArr = latLng.split(",");
                Location place = new Location("");
                place.setLatitude(Double.parseDouble(latLngArr[0]));
                place.setLongitude(Double.parseDouble(latLngArr[1]));
                Log.e("dis", place.distanceTo(location) + "");
                if (place.distanceTo(location) < shortest) {
                    shortest = place.distanceTo(location);
                    lat = Double.parseDouble(latLngArr[0]);
                    lng = Double.parseDouble(latLngArr[1]);
                    name = Constants.locationName.get(t);
                }
                UniqLocation uniqLocation = new UniqLocation();
                uniqLocation.setLatLng(Constants.locationGeo.get(t));
                uniqLocation.setPlace_name(Constants.locationName.get(t));
                dbHepler.insertUniqPlace(uniqLocation);
            }

            if(dbHepler.getUniqLocation("" + currentLatitude + "," + currentLongitude) != null){

                UniqLocation ul = dbHepler.getUniqLocation("" + currentLatitude + "," + currentLongitude);
                TextView x = new TextView(MainActivity.this);
                x.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                x.setText("Your current location: \n" + ul.getLatLng() + "\n" + ul.getPlace_name() + "\n Your next predicted locations are listed bellow\n");
                binding.container.llInfo.addView(x);

//            googleMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng))
//                    .title(name));
                SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
                Date d = new Date();
                String dayOfTheWeek = sdf.format(d);
                Calendar c = Calendar.getInstance();
                int Hr24 = c.get(Calendar.HOUR_OF_DAY);
                Hr24++;
                if (Hr24 == 24) {
                    Hr24 = 1;
                }

                ArrayList<String> latlng = new ArrayList<>();
                List<PredictedLocation> pl = dbHepler.getPredectLocationByGroup(Hr24);

                float max = 600000;
                Marker m = null;
                Location locationHistory = null;
                String day = "";
                int time = 1;
                for (int t = 0; t < pl.size(); t++) {
                    if (dayOfTheWeek.equalsIgnoreCase("monday")) {
                        if (latlng.contains(pl.get(t).getMONDAY())) {

                        } else {
                            latlng.add(pl.get(t).getMONDAY());
                            String arr [] = pl.get(t).getMONDAY().split(", ");
                            LatLng predectedLocation = new LatLng(Double.parseDouble(arr[0]), Double.parseDouble(arr[1]));
                            Location l = new Location("");
                            l.setLatitude(predectedLocation.latitude);
                            l.setLongitude(predectedLocation.longitude);
                            if(l.distanceTo(location) > 115 && l.distanceTo(location) < max){
                                max = l.distanceTo(location);
                                locationHistory = l;
                                time = pl.get(t).getTime();
                                day = "monday";
                                TextView tv = new TextView(MainActivity.this);
                                //tv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                                tv.setText(pl.get(t).getMONDAY() + "\n");
                                binding.container.llInfo.addView(tv);
                                if(m != null){
                                    m.remove();
                                }
                                m = googleMap.addMarker(new MarkerOptions().position(predectedLocation)
                                        .title("Predicted Locations"));
                            }
                        }
                    } else if (dayOfTheWeek.equalsIgnoreCase("tuesday")) {
                        if (latlng.contains(pl.get(t).getTUESDAY())) {

                        } else {
                            latlng.add(pl.get(t).getTUESDAY());
                            String arr [] = pl.get(t).getTUESDAY().split(", ");
                            LatLng predectedLocation = new LatLng(Double.parseDouble(arr[0]), Double.parseDouble(arr[1]));
                            Location l = new Location("");
                            l.setLatitude(predectedLocation.latitude);
                            l.setLongitude(predectedLocation.longitude);
                            Log.e("predic", l.distanceTo(location) + "");
                            if(l.distanceTo(location) > 115 && l.distanceTo(location) < max){
                                max = l.distanceTo(location);
                                locationHistory = l;
                                time = pl.get(t).getTime();
                                day = "tuesday";
                                TextView tv = new TextView(MainActivity.this);
                                //tv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                                tv.setText(pl.get(t).getTUESDAY() + "\n");
                                binding.container.llInfo.addView(tv);
                                if(m != null){
                                    m.remove();
                                }
                                m = googleMap.addMarker(new MarkerOptions().position(predectedLocation)
                                        .title("Predicted Locations"));
                            }
                        }
                    } else if (dayOfTheWeek.equalsIgnoreCase("wednesday")) {
                        if (latlng.contains(pl.get(t).getWEDNESDAY())) {

                        } else {
                            latlng.add(pl.get(t).getWEDNESDAY());
                            String arr [] = pl.get(t).getWEDNESDAY().split(", ");
                            LatLng predectedLocation = new LatLng(Double.parseDouble(arr[0]), Double.parseDouble(arr[1]));
                            Location l = new Location("");
                            l.setLatitude(predectedLocation.latitude);
                            l.setLongitude(predectedLocation.longitude);
                            if(l.distanceTo(location) > 115 && l.distanceTo(location) < max){
                                max = l.distanceTo(location);
                                locationHistory = l;
                                time = pl.get(t).getTime();
                                day = "wednesday";
                                TextView tv = new TextView(MainActivity.this);
                                //tv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                                tv.setText(pl.get(t).getWEDNESDAY() + "\n");
                                binding.container.llInfo.addView(tv);
                                if(m != null){
                                    m.remove();
                                }
                                m = googleMap.addMarker(new MarkerOptions().position(predectedLocation)
                                        .title("Predicted Locations"));
                            }
                        }
                    } else if (dayOfTheWeek.equalsIgnoreCase("thursday")) {
                        if (latlng.contains(pl.get(t).getTHURSDAY())) {

                        } else {
                            latlng.add(pl.get(t).getTHURSDAY());
                            String arr [] = pl.get(t).getTHURSDAY().split(", ");
                            LatLng predectedLocation = new LatLng(Double.parseDouble(arr[0]), Double.parseDouble(arr[1]));
                            Location l = new Location("");
                            l.setLatitude(predectedLocation.latitude);
                            l.setLongitude(predectedLocation.longitude);
                            if(l.distanceTo(location) > 115 && l.distanceTo(location) < max){
                                max = l.distanceTo(location);
                                locationHistory = l;
                                time = pl.get(t).getTime();
                                day = "thrusday";
                                TextView tv = new TextView(MainActivity.this);
                                //tv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                                tv.setText(pl.get(t).getTHURSDAY() + "\n");
                                binding.container.llInfo.addView(tv);
                                if(m != null){
                                    m.remove();
                                }
                                m = googleMap.addMarker(new MarkerOptions().position(predectedLocation)
                                        .title("Predicted Locations"));
                            }
                        }
                    } else if (dayOfTheWeek.equalsIgnoreCase("friday")) {
                        if (latlng.contains(pl.get(t).getFRIDAY())) {

                        } else {
                            latlng.add(pl.get(t).getFRIDAY());
                            String arr [] = pl.get(t).getFRIDAY().split(", ");
                            LatLng predectedLocation = new LatLng(Double.parseDouble(arr[0]), Double.parseDouble(arr[1]));
                            Location l = new Location("");
                            l.setLatitude(predectedLocation.latitude);
                            l.setLongitude(predectedLocation.longitude);
                            if(l.distanceTo(location) > 115 && l.distanceTo(location) < max){
                                max = l.distanceTo(location);
                                locationHistory = l;
                                time = pl.get(t).getTime();
                                day = "friday";
                                TextView tv = new TextView(MainActivity.this);
                                //tv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                                tv.setText(pl.get(t).getFRIDAY() + "\n");
                                binding.container.llInfo.addView(tv);
                                if(m != null){
                                    m.remove();
                                }
                                m = googleMap.addMarker(new MarkerOptions().position(predectedLocation)
                                        .title("Predicted Locations"));
                                Log.e("max", max + "");
                            }
                        }
                    } else if (dayOfTheWeek.equalsIgnoreCase("saturday")) {
                        if (latlng.contains(pl.get(t).getSATERDAY())) {

                        } else {
                            latlng.add(pl.get(t).getSATERDAY());
                            String arr [] = pl.get(t).getSATERDAY().split(", ");
                            LatLng predectedLocation = new LatLng(Double.parseDouble(arr[0]), Double.parseDouble(arr[1]));
                            Location l = new Location("");
                            l.setLatitude(predectedLocation.latitude);
                            l.setLongitude(predectedLocation.longitude);
                            if(l.distanceTo(location) > 115 && l.distanceTo(location) < max){
                                max = l.distanceTo(location);
                                locationHistory = l;
                                time = pl.get(t).getTime();
                                day = "saturday";
                                TextView tv = new TextView(MainActivity.this);
                                //tv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                                tv.setText(pl.get(t).getSATERDAY() + "\n");
                                binding.container.llInfo.addView(tv);
                                if(m != null){
                                    m.remove();
                                }
                                m = googleMap.addMarker(new MarkerOptions().position(predectedLocation)
                                        .title("Predicted Locations"));
                            }
                        }
                    } else if (dayOfTheWeek.equalsIgnoreCase("sunday")) {
                        if (latlng.contains(pl.get(t).getSUNDAY())) {

                        } else {
                            latlng.add(pl.get(t).getSUNDAY());
                            String arr [] = pl.get(t).getSUNDAY().split(", ");
                            LatLng predectedLocation = new LatLng(Double.parseDouble(arr[0]), Double.parseDouble(arr[1]));
                            Location l = new Location("");
                            l.setLatitude(predectedLocation.latitude);
                            l.setLongitude(predectedLocation.longitude);
                            if(l.distanceTo(location) > 115 && l.distanceTo(location) < max){
                                max = l.distanceTo(location);
                                locationHistory = l;
                                time = pl.get(t).getTime();
                                day = "sunday";
                                TextView tv = new TextView(MainActivity.this);
                                //tv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                                tv.setText(pl.get(t).getSUNDAY() + "\n");
                                binding.container.llInfo.addView(tv);
                                if(m != null){
                                    m.remove();
                                }
                                m = googleMap.addMarker(new MarkerOptions().position(predectedLocation)
                                        .title("Predicted Locations"));
                            }
                        }
                    }


                }
                PredictedLocationHistory predictedLocationHistory= new PredictedLocationHistory();
                predictedLocationHistory.setDay(day);
                predictedLocationHistory.setLatlng(locationHistory.getLatitude() + "," + locationHistory.getLongitude());
                predictedLocationHistory.setTime(time);
                dbHepler.insertPredectedLocationHistory(predictedLocationHistory);
            }else {
                String add = getAddress(currentLatitude, currentLongitude);
                UniqLocation uniqLocation = new UniqLocation();
                uniqLocation.setLatLng("" + currentLatitude + "," + currentLongitude);
                uniqLocation.setPlace_name(add);
                dbHepler.insertUniqPlace(uniqLocation);
                //if(getIntent().getBooleanExtra("hide", false) == true){
                    Toast.makeText(this, "Your current location is new. Restart this app and you will be able to see your predicted location.", Toast.LENGTH_LONG).show();
                //}
            }

        }
    }

    public String getAddress(double lat, double lng) {
        Geocoder geocoder = new Geocoder(MainActivity.this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 10);
            Address obj = addresses.get(0);
            String add = obj.getAddressLine(0) + ", " + obj.getCountryName() + ", " + obj.getLocality() + ", " + obj.getFeatureName() + ", " + obj.getAddressLine(1);
            Log.e("IGA", "Address" + add);
            return add;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            return "null";
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
                    /*
                     * Thrown if Google Play services canceled the original
                     * PendingIntent
                     */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
                /*
                 * If no resolution is available, display a dialog to the
                 * user with the error.
                 */
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
        Log.e("change", "location changed");

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.REQUEST_ACCESSFINELOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                readLocation(location);
            } else {
                Toast.makeText(this, "You Must Have To Allow These Permissions", Toast.LENGTH_SHORT).show();
                finish();
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Now lets connect to the API
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e(this.getClass().getSimpleName(), "onPause()");

        //Disconnect from API onPause()
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Add a marker in Sydney, Australia,
        // and move the map's camera to the same location.
        IS_MAP_READY = true;
        this.googleMap = googleMap;
    }
}
