package triumphit.tech.plg.activity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import triumphit.tech.plg.R;
import triumphit.tech.plg.services.Background;
import triumphit.tech.plg.utils.Constants;

public class LandingActivity extends AppCompatActivity{

    Button pl, his, serv, internal, shwpre;
    TextView welcome;

    public static Background service;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_landing);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("PLG");
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();

        Typeface face;
        face = Typeface.createFromAsset(getAssets(), "pasajero.ttf");

        Constants.initPreUniqLocation(this);
        Constants.initPredictedLocation(this);

        welcome = (TextView) findViewById(R.id.tv_welcome);
        pl = (Button) findViewById(R.id.bt_detect_location);
        his = (Button) findViewById(R.id.bt_history);
        internal = (Button) findViewById(R.id.bt_internal);
        shwpre = (Button) findViewById(R.id.bt_prediction);
        serv = (Button) findViewById(R.id.bt_service);

//        welcome.setTypeface(face);

//        pl.setTypeface(face);
//        his.setTypeface(face);
//        internal.setTypeface(face);
//        shwpre.setTypeface(face);
//        serv.setTypeface(face);

        if(isMyServiceRunning(Background.class)){
            serv.setText("Stop Service");
        }else {
            serv.setText("Start Service");
        }


        serv.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                if(isMyServiceRunning(Background.class)){
                    stopService(new Intent(LandingActivity.this, Background.class));
                    serv.setText("Start Service");
                }else {
                    startService(new Intent(LandingActivity.this, Background.class));
                    serv.setText("Stop Service");
                }

            }
        });
        internal.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                startActivity(new Intent(LandingActivity.this, HistoryActivity.class).putExtra("task", "internal"));
            }
        });
        shwpre.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                startActivity(new Intent(LandingActivity.this, MainActivity.class).putExtra("hide", true));
            }
        });
        his.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                startActivity(new Intent(LandingActivity.this, HistoryActivity.class).putExtra("task", "history"));
            }
        });
        pl.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                startActivity(new Intent(LandingActivity.this, MainActivity.class).putExtra("hide", false));
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}
