package triumphit.tech.plg.utils;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import triumphit.tech.plg.db.PredictedLocation;
import triumphit.tech.plg.db.UniqLocation;

/**
 * Created by 02691 on 7/monday.add("/2017.
 */

public class Constants {

    public static final int REQUEST_ACCESSFINELOCATION = 1;
    public static final float MAP_ZOOM_LEVEL = 18;
    public static ArrayList<String> locationName;
    public static ArrayList<String> locationGeo;

    public static void initPreUniqLocation(Context context) {
        locationName = new ArrayList<>();
        locationGeo = new ArrayList<>();

        locationName.add("Basa");
        locationName.add("kakoli");
        locationName.add("jasimuddinav");
        locationName.add("bracub2");
        locationName.add("bracub03");
        locationName.add("vooteradda");
        locationName.add("masjid");
        locationName.add("maskotplaza");
        locationName.add("khilkhet");
        locationName.add("amtoli");
        locationName.add("airportroad");
        locationName.add("azampurbridge");
        locationName.add("sheora");
        locationName.add("mina bazarsector6");
        locationName.add("bisshoroad");
        locationName.add("soinikclub");
        locationName.add("maasrangatelevision");
        locationName.add("alfrescobanani");
        locationName.add("bananibasa");
        locationName.add("dhanmondi32");
        locationName.add("hazaribaghbasa");
        locationName.add("kulalmoholmasjid");
        locationName.add("nawabganjbazar");
        locationName.add("zigatolazero point");
        locationName.add("dhanmondi4A");
        locationName.add("bijoysarani");
        locationName.add("ashkona");
        locationName.add("mirpur10");
        locationName.add("kalshibusstand");
        locationName.add("pirerbaghmirpur");
        locationName.add("banani11");
        locationName.add("wirelessmohakhali");
        locationName.add("TNTmath");
        locationName.add("gulshan1dccmarket");
        locationName.add("gloriajeans");
        locationName.add("rajlakshmibusstand");
        locationName.add("navalheadquarter");
        locationName.add("cinemon");
        locationName.add("radison");
        locationName.add("sankarbusstand");
        locationName.add("abahoni");
        locationName.add("sciencelab");
        locationName.add("bdr bazar");
        locationName.add("kublaikhansector7");
        locationName.add("mirpur2");
        locationName.add("sangsadbhaban");
        locationName.add("royalres");
        locationName.add("shahidmanir");
        locationName.add("fullerroad");
        locationName.add("nannabiriyani");
        locationName.add("seashell");

        locationGeo.add("23.870263,90.405461");
        locationGeo.add("23.793778,90.400865");
        locationGeo.add("23.860621,90.4002307");
        locationGeo.add("23.780162,90.407110");
        locationGeo.add("23.780780,90.407300");
        locationGeo.add("23.865919,90.399890");
        locationGeo.add("23.872092,90.403405");
        locationGeo.add("23.873986,90.399948");
        locationGeo.add("23.829766,90.419813");
        locationGeo.add("23.780635,90.398915");
        locationGeo.add("23.852519,90.406924");
        locationGeo.add("23.867904,90.400450");
        locationGeo.add("23.818274,90.414163");
        locationGeo.add("23.868165,90.402868");
        locationGeo.add("23.821146,90.418671");
        locationGeo.add("23.791632,90.400647");
        locationGeo.add("23.800483,90.402040");
        locationGeo.add("23.791028,90.404119");
        locationGeo.add("23.783537,90.402185");
        locationGeo.add("23.773949,90.378013");
        locationGeo.add("23.726686,90.372434");
        locationGeo.add("23.727554,90.371769");
        locationGeo.add("23.23172,90.376942");
        locationGeo.add("23.739499,90.36723");
        locationGeo.add("23.740849,90.374944");
        locationGeo.add("23.764761,90.385584");
        locationGeo.add("23.851765,90.417287");
        locationGeo.add("23.791309,90.348452");
        locationGeo.add("23.821925,90.377633");
        locationGeo.add("23.789269,90.365972");
        locationGeo.add("23.790691,90.405485");
        locationGeo.add("23.780648,90.405582");
        locationGeo.add("23.784363,90.404172");
        locationGeo.add("23.779526,90.415577");
        locationGeo.add("23.779199,90.416460");
        locationGeo.add("23.864358,90.399913");
        locationGeo.add("23.803361,90.402461");
        locationGeo.add("23.780243,90.406818");
        locationGeo.add("23.816859,90.408567");
        locationGeo.add("23.750590,90.368375");
        locationGeo.add("23.749123,90.369466");
        locationGeo.add("23.738823,90.383434");
        locationGeo.add("23.868570,90.400642");
        locationGeo.add("23.869489,90.391664");
        locationGeo.add("23.806961,90.366587");
        locationGeo.add("23.758213,90.374352");
        locationGeo.add("23.719693,90.390503");
        locationGeo.add("23.727710,90.396658");
        locationGeo.add("23.728775,90.392511");
        locationGeo.add("23.716087,90.400274");
        locationGeo.add("23.864215,90.400250");

        List<UniqLocation> ul = new ArrayList<>();
        for (int t = 0; t < locationName.size(); t++) {
            UniqLocation uniqLocation = new UniqLocation();
            uniqLocation.setPlace_name(locationName.get(t));
            uniqLocation.setLatLng(locationGeo.get(t));
            ul.add(uniqLocation);
        }

        DBHepler dbHepler = new DBHepler(context);
        dbHepler.insertLocationAsList(ul);

    }

    public static void initPredictedLocation(Context context) {
        ArrayList<Integer> time = new ArrayList<>();
        ArrayList<String> sunday = new ArrayList<>();
        ArrayList<String> monday = new ArrayList<>();
        ArrayList<String> tuesday = new ArrayList<>();
        ArrayList<String> wednesday = new ArrayList<>();
        ArrayList<String> thrusday = new ArrayList<>();
        ArrayList<String> friday = new ArrayList<>();
        ArrayList<String> saterday = new ArrayList<>();

        List<PredictedLocation> predictedLocations = new ArrayList<>();

        time.add(0);
        time.add(1);
        time.add(2);
        time.add(3);
        time.add(4);
        time.add(5);
        time.add(6);
        time.add(7);
        time.add(8);
        time.add(9);
        time.add(10);
        time.add(11);
        time.add(12);
        time.add(13);
        time.add(14);
        time.add(15);
        time.add(16);
        time.add(17);
        time.add(18);
        time.add(19);
        time.add(20);
        time.add(21);
        time.add(22);
        time.add(23);

       
        DBHepler dbHepler = new DBHepler(context);

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(context.getApplicationContext().getAssets().open("data.txt")));
            String line;
            int c = 0;
            String dayName = "MONDAY";
            while ((line = reader.readLine()) != null) {
                //Log.e("code", line);
                if(line.startsWith("monday")){
                    monday.add(line.replace("monday", ""));
                }else if(line.startsWith("tuesday")){
                    tuesday.add(line.replace("tuesday", ""));
                }else if(line.startsWith("wednesday")){
                    wednesday.add(line.replace("wednesday", ""));
                }else if(line.startsWith("thursday")){
                    thrusday.add(line.replace("thursday", ""));
                }else if(line.startsWith("friday")){
                    friday.add(line.replace("friday", ""));
                }else if(line.startsWith("saturday")){
                    saterday.add(line.replace("saturday", ""));
                }else if(line.startsWith("sunday")){
                    sunday.add(line.replace("sunday", ""));
                }

            }

            int timeHour = 0;
            for (int t = 0; t < monday.size(); t++) {
                PredictedLocation predictedLocation = new PredictedLocation();
                predictedLocation.setTime(timeHour);

                timeHour++;
                if(timeHour == 24){
                    timeHour = 0;
                }

                predictedLocation.setId((long)t);
                predictedLocation.setTime(timeHour);
                predictedLocation.setMONDAY(monday.get(t));
                predictedLocation.setTUESDAY(tuesday.get(t));
                predictedLocation.setWEDNESDAY(wednesday.get(t));
                predictedLocation.setTHURSDAY(thrusday.get(t));
                predictedLocation.setFRIDAY(friday.get(t));
                predictedLocation.setSATERDAY(saterday.get(t));
                predictedLocation.setSUNDAY(sunday.get(t));

                predictedLocations.add(predictedLocation);
            }

            dbHepler.insertPredectLocationAsList(predictedLocations);
            //Log.e("predecti", dbHepler.getPredectLocationAsList().size() + "");

//            List<PredictedLocation> l = dbHepler.getPredectLocationAsList();
//            for (int t = 0; t < l.size(); t++) {
//                Log.e("time", l.get(t).getTime() + "");
//            }

            //Log.e("data", monday.size() + " " + tuesday.size() + " " + wednesday.size() + " " + thrusday.size() + " " + friday.size() + " " + saterday.size() + " " + sunday.size());

        } catch (IOException ex) {
            Log.e("io exp", ex.toString());
        }

    }

}
