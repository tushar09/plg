package triumphit.tech.plg.utils;

/**
 * Created by Tushar on 8/1/2017.
 */

public class Extractor{
    String time, latlng, dayName;

    public String getTime(){
        return time;
    }

    public void setTime(String time){
        this.time = time;
    }

    public String getLatlng(){
        return latlng;
    }

    public void setLatlng(String latlng){
        this.latlng = latlng;
    }

    public String getDayName(){
        return dayName;
    }

    public void setDayName(String dayName){
        this.dayName = dayName;
    }
}
