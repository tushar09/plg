package triumphit.tech.plg.utils;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.List;

import triumphit.tech.plg.db.DaoMaster;
import triumphit.tech.plg.db.DaoSession;
import triumphit.tech.plg.db.PredictedLocation;
import triumphit.tech.plg.db.PredictedLocationDao;
import triumphit.tech.plg.db.PredictedLocationHistory;
import triumphit.tech.plg.db.PredictedLocationHistoryDao;
import triumphit.tech.plg.db.UniqLocation;
import triumphit.tech.plg.db.UniqLocationDao;

/**
 * Created by 02691 on 7/24/2017.
 */

public class DBHepler {

    private final String DB_NAME = "plg-db";
    private final DaoSession daoSession;
    private DaoMaster daoMaster;
    private UniqLocationDao uniqLocationDao;
    private PredictedLocationDao predictedLocationDao;
    private PredictedLocationHistoryDao predictedLocationHistoryDao;

    private SQLiteOpenHelper sqLiteOpenHelper;

    private Context context;
    public DBHepler(Context context) {
        this.context = context;
        sqLiteOpenHelper = new DaoMaster.DevOpenHelper(context, DB_NAME, null);
        daoMaster = new DaoMaster(sqLiteOpenHelper.getWritableDatabase());
        daoSession = daoMaster.newSession();
        uniqLocationDao = daoSession.getUniqLocationDao();
        predictedLocationDao = daoSession.getPredictedLocationDao();
        predictedLocationHistoryDao = daoSession.getPredictedLocationHistoryDao();
    }

    public void insertUniqPlace(UniqLocation con){
        uniqLocationDao.insertOrReplaceInTx(con);
    }

    public List<UniqLocation> getLocatioAsList() {
        return uniqLocationDao.queryBuilder().list();
    }

    public UniqLocation getUniqLocation(String latlng) {
        Log.e("db", latlng);
        return uniqLocationDao.queryBuilder().where(UniqLocationDao.Properties.LatLng.eq(latlng)).unique();
    }

    public void insertLocationAsList(List<UniqLocation> ul){
        uniqLocationDao.insertOrReplaceInTx(ul);
    }

    public void insertPredectLocationAsList(List<PredictedLocation> predictedLocations){
        predictedLocationDao.insertOrReplaceInTx(predictedLocations);
    }

    public List<PredictedLocation> getPredectLocationAsList(){
        return predictedLocationDao.queryBuilder().list();
    }

    public List<PredictedLocation> getPredectLocationByGroup(int time){
        //predictedLocationDao.queryBuilder().where(PredictedLocationDao.Properties.Time.eq(time)).where(PredictedLocationDao.Properties.)
        return predictedLocationDao.queryBuilder().where(PredictedLocationDao.Properties.Time.eq(time)).list();
    }

    public void insertPredectedLocationHistory(PredictedLocationHistory predictedLocationHistory){
        predictedLocationHistoryDao.insert(predictedLocationHistory);
    }

    public List<PredictedLocationHistory> getPredectedLocationHistoryAsList(){
        return predictedLocationHistoryDao.queryBuilder().list();
    }

}
