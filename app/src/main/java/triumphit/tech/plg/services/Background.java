package triumphit.tech.plg.services;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.TrafficStats;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.google.android.gms.location.LocationListener;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import triumphit.tech.plg.R;
import triumphit.tech.plg.activity.LandingActivity;
import triumphit.tech.plg.activity.MainActivity;
import triumphit.tech.plg.db.UniqLocation;
import triumphit.tech.plg.utils.DBHepler;

public class Background extends Service{
    public static Notification notification;
    NotificationCompat.Builder builder;
    private static Handler handler;
    private static Runnable updateTask;
    private static boolean handlerCheck = false;
    PendingIntent pendingIntent;
    PendingIntent contentIntent;
    SharedPreferences sp;
    SharedPreferences.Editor editor;

    Context context;
    private LocationManager mLocationManager;
    private DBHepler dbHepler;

    @Override
    public IBinder onBind(Intent intent){
        return null;
    }

    @Override
    public void onCreate(){
        super.onCreate();
        context = this;
        startInBackground();
        contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, LandingActivity.class), 0);
        dbHepler = new DBHepler(this);
        sp = getSharedPreferences("stat", Context.MODE_PRIVATE);
        editor = sp.edit();

    }

    private void startInBackground(){
        Log.e("First", "First");
        Intent myIntent = new Intent(this, LandingActivity.class);
        pendingIntent = PendingIntent.getActivity(this, 0, myIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        mLocationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLocationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER, 1000, 10F,
                mLocationListeners[0]);

        builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_background_service)
                .setContentTitle("PLG IS RUNNING")
                .setContentText("Click for actions")
                .setContentIntent(pendingIntent);
        handler = new Handler();
        updateTask = new Runnable(){
            @Override
            public void run(){
                update();
                if(handler != null){
                    handler.postDelayed(this, 3000);
                }

            }
        };
        if(!handlerCheck){
            handler.postDelayed(updateTask, 0);
            handlerCheck = true;
        }
        startForeground(1, builder.build());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        super.onStartCommand(intent, flags, startId);

        return START_STICKY;
    }

    void update(){
        if(mLastLocation != null){
            Log.e("started", mLastLocation.getLatitude() + "");
            UniqLocation uniqLocation = new UniqLocation();
            uniqLocation.setLatLng(mLastLocation.getLatitude() + "," + mLastLocation.getLongitude());
            uniqLocation.setPlace_name(getAddress(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
            dbHepler.insertUniqPlace(uniqLocation);
        }else {
            Log.e("started", "null");
        }

    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        stopSelf();
        handlerCheck = false;
        handler = null;
        updateTask = null;
        freeMemory();
        //startService(new Intent(getApplicationContext(), Background.class));
    }

    public void freeMemory(){
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
    }

    Location mLastLocation;
    private class LocationListener implements android.location.LocationListener{
        //Location mLastLocation;

        public LocationListener(String provider){
            Log.e("loc", "LocationListener " + provider);
            mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location){
            Log.e("loc", "onLocationChanged: " + location);
            mLastLocation.set(location);
        }

        @Override
        public void onProviderDisabled(String provider){
            Log.e("loc", "onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider){
            Log.e("loc", "onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras){
            Log.e("loc", "onStatusChanged: " + provider);
        }
    }

    LocationListener[] mLocationListeners = new LocationListener[]{
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };

    public String getAddress(double lat, double lng) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 10);
            Address obj = addresses.get(0);
            String add = obj.getAddressLine(0) + ", " + obj.getCountryName() + ", " + obj.getLocality() + ", " + obj.getFeatureName() + ", " + obj.getAddressLine(1);
            Log.e("IGA", "Address" + add);
            return add;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            return "null";
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            return "Place name not found";
        }
    }

}
